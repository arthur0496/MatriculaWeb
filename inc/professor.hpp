#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include<string>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa{
private:
	string formaçao;
	string departamento;
	float indice_de_aprovaçao;
public:
	Professor();
	~Professor();
	string getFormaçao();
	void setFormaçao(string formaçao);
	string getDepartamento();
	void setDeparttamento(string departamento);
}
#ifend
