#ifndef PESSOA.HPP
#define PESSOA.HPP

#include<string>;

using namespace std;

class Pessoa {
private:
	int matricula;
	string nome;
	long int cpf;
	string telefone;
	string endereço;
public
	Pessoa();
	~Pessoa();
	int getMatricula();
	void setMatricula(int matricula);
	string getNome();
	void setNome(string nome);
	long int getCpf();
	void setCpf (long int cpf);
	string getTelefone();
	void setTelefone (string telefone);
	string getEndereço();
	void setEndereço(string endereço);
	
}
#endif
