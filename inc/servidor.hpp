#ifndef SERVIDOR_HPP
#define SERVIDOR_HPP

#include<string>
#include"pessoa.hpp"

using namespace std;

class Servidor : public Pessoa{
private:
	string cargo;
	string departamento;
	int carga_horaria;
public:
	Servidor();
	~Servidor();
	string getCargo();
	void setCargo(string cargo);
	string get departamento();
	void setDepartamento(string departamento);
	int getCarga_horaria();
	void setCarga_horaria(int carga_horaria);
}
#endif
